#!/usr/bin/env python3

import argparse
from logzero import logger
import math
import os
import osmium
import pathlib
import requests
import re
import sys
import time
import zipfile
from multiprocessing import Pool
from math import sin, cos, pi, sqrt
from vendor.gpsconverter.wgs84_ch1903 import GPSConverter

class AddressStorage():
    """
    This class stores address information.

    For each occuring address (=combination of streetname + housenumber),
    a list entry with lat, lon is created. If the same combination occurs
    multiple times, additional coordinates entries will be added to the list.

    Using exists() one can query if an address already exists near the given
    pair of coordinates. You can specify the distance using "max_dist".
    """
    def __init__(self):
        self.max_dist = 250 # meters
        self.data = dict()

        self._init_regex()
        self._coord_converter = GPSConverter()

    def merge(self, other):
        for addrhash, coord_list in other.data.items():
            if addrhash in self.data:
                self.data[addrhash] += coord_list
            else:
                self.data[addrhash] = coord_list

    def add(self, addr):
        addrhash = (self._normalize(addr['street']),
               self._normalize(addr['housenumber']))
        if addrhash not in self.data.keys():
            self.data[addrhash] = list()
        coords = {"lat": addr['lat'], "lon": addr['lon']}
        self.data[addrhash].append(coords)

    def _normalize(self, text):
        text = text.replace("ß", "SS")
        text = text.upper()
        text = text.replace("Ä", "AE")
        text = text.replace("Ö", "OE")
        text = text.replace("Ü", "UE")
        text = text.replace("É", "E")
        text = text.replace("È", "E")
        text = text.replace("À", "A")
        text = text.replace(" ", "")
        text = text.replace("-", "")
        text = self.trimmer.match(text).group(1)
        return text

    def _init_regex(self):
        regex = r"\s*(\S*)\s*" # = don't match whitespace at beginning and end
        self.trimmer = re.compile(regex)

    def num_addresses(self):
        return len(self.data)

    def num_individual_addresses(self):
        count = 0
        for address, coordinates_list in self.data.items():
            count += len(coordinates_list)
        return count

    def exists(self, candidate) -> bool:
        addrhash = (self._normalize(candidate['street']),
                    self._normalize(candidate['housenumber']))
        if addrhash not in self.data.keys():
            return False
        else:
            for location in self.data[addrhash]:
                if self._within_tolerance(location['lat'], candidate['lat'],
                                          location['lon'], candidate['lon'],
                                          self.max_dist):
                    return True
        return False

    def _distance(self, lat1, lon1, lat2, lon2):
        x1 = self._coord_converter.WGStoCHx(lat=lat1, lng=lon1)
        x2 = self._coord_converter.WGStoCHx(lat=lat2, lng=lon2)
        y1 = self._coord_converter.WGStoCHy(lat=lat1, lng=lon1)
        y2 = self._coord_converter.WGStoCHy(lat=lat2, lng=lon2)
        return math.sqrt((x1-x2)**2 + (y1-y2)**2)

    def _within_tolerance(self, lat1, lat2, lon1, lon2, tolerance):
        if self._distance(lat1, lon1, lat2, lon2) < tolerance:
            return True
        return False

class GwrHandler(osmium.SimpleHandler):
    """
    This class reads, filters and writes GWR data.
    """
    def __init__(self, storage: AddressStorage, writer):
        super(GwrHandler, self).__init__()

        self.storage = storage
        self.writer = writer

        self.stats = dict()
        self.stats['num_total'] = 0
        self.stats['num_invalid_loc'] = 0
        self.stats['num_invalid_noaddr'] = 0
        self.stats['num_invalid_dot'] = 0
        self.stats['num_invalid_object'] = 0
        self.stats['num_existing'] = 0
        self.stats['num_nonexisting'] = 0

    def node(self, n):
        self.stats['num_total'] += 1

        if not n.location.valid():
            self.stats['num_invalid_loc'] += 1
            return

        street = n.tags.get("addr:street")
        housenumber = n.tags.get("addr:housenumber")
        if not (street and housenumber):
            self.stats['num_invalid_noaddr'] += 1
            return

        if "." in housenumber:
            self.stats['num_invalid_dot'] += 1
            return

        if housenumber == '[object Object]':
            self.stats['num_invalid_object'] += 1
            return

        myaddr = {"street": street, "housenumber": housenumber,
                  "lat": n.location.lat, "lon": n.location.lon}

        if self.storage.exists(myaddr):
            self.stats['num_existing'] += 1
        else:
            self.stats['num_nonexisting'] += 1
            self.writer.add_node(n)

class OsmReader(osmium.SimpleHandler):
    """
    This class reads OSM data
    """
    def __init__(self):
        super(OsmReader, self).__init__()
        self.storage = AddressStorage()
        self._worker_id = None
        self._num_workers = None

    def node(self, n):
        if not n.id % self._num_workers == self._worker_id:
            return

        if not n.location.valid():
            return

        entries = self._extract_addresses(n)
        for entry in entries:
            entry.update({"lat": n.location.lat, "lon": n.location.lon})
            self.storage.add(entry)

    def way(self, w):
        if not w.id % self._num_workers == self._worker_id:
            return

        if not is_valid(w):
            return

        entries = self._extract_addresses(w)
        for entry in entries:
            entry.update(centroid(w))
            self.storage.add(entry)

    @staticmethod
    def _extract_addresses(osmobj):
        street = osmobj.tags.get("addr:street")
        place = osmobj.tags.get("addr:place")
        housenumber = osmobj.tags.get("addr:housenumber")
        addresses = list()
        import requests
        if street and housenumber:
            addresses.append({"street": street, "housenumber": housenumber})
        if place and housenumber:
            addresses.append({"street": place, "housenumber": housenumber})
        return addresses

    def work(self, worker_id: int, num_workers: int, filename: str):
        self._worker_id = worker_id
        self._num_workers = num_workers
        start = time.monotonic()
        self.apply_file(filename=filename, locations=True)
        stop = time.monotonic()
        return self.storage

def is_valid(way):
    return all([node.location.valid() for node in way.nodes])

def truncate(num):
    return '%.2f' % num

def centroid(way):
    """
    The centroid of a way is cheaply calculated as the mean of all node
    coordinates.
    """
    lat = 0
    lon = 0
    count = 0
    for n in way.nodes:
        lat += n.location.lat
        lon += n.location.lon
        count += 1

    lat /= count
    lon /= count

    return ({"lat": lat, "lon": lon})

def work(worker_id: int, num_workers: int, filename: str):
    osm_reader = OsmReader()
    return osm_reader.work(worker_id, num_workers, filename)

def download_from_overpass(municipality_ids, filepath):
    """
    Download existing OSM address data to specified path
    """
    query = '('
    for municipality_id in municipality_ids:
        query += f'area["swisstopo:BFS_NUMMER"="{municipality_id}"];'
    query += ') -> .myarea;'
    query += 'node[~"addr:(street|place)"~"."]["addr:housenumber"](area.myarea); out qt;'
    query += 'way[~"addr:(street|place)"~"."]["addr:housenumber"](area.myarea); (._; >;); out qt;'

    # Change output type from default (='body') to 'meta' in order to be able
    # to open Overpass output in JOSM
    #query += 'node[~"addr:(street|place)"~"."]["addr:housenumber"](area.myarea); out meta qt;'
    #query += 'way[~"addr:(street|place)"~"."]["addr:housenumber"](area.myarea); (._; >;); out meta qt;'

    logger.debug('Going to use the following Overpass query:')
    logger.debug(query)

    url = f'https://overpass-api.de/api/interpreter?data={query}'
    r = requests.get(url)
    if r.status_code != 200:
        raise RuntimeError(f'Downloading {url} failed with status code={r.status_code}')
    open(filepath, 'wb').write(r.content)

    # TODO: use https://python-overpy.readthedocs.io/en/latest/index.html ?

def download_from_gwr(municipality_ids, TMPDIR):
    """
    Download GWR data
    """
    # TODO: create directorie(s) according to https://stackoverflow.com/a/14364249
    for municipality_id in municipality_ids:
        url=f'https://qa.poole.ch/addresses/GWR/{municipality_id}.osm.zip'
        r = requests.get(url)
        if r.status_code != 200:
            raise RuntimeError(f'Downloading {url} failed with status code={r.status_code}')
        # write download into zipfile
        zippath = bfs2zippath(municipality_id, TMPDIR)
        osmpath = bfs2osmpath(municipality_id, TMPDIR)
        open(zippath, 'wb').write(r.content)

        # extract file from zipfile
        with zipfile.ZipFile(zippath, 'r') as zipfile_:
            zipfile_.extract(f'{municipality_id}.osm', TMPDIR)


def bfs2osmpath(bfs, TMPDIR):
    """Convert BFS ID to full path."""
    return f'{TMPDIR}/{bfs}.osm'

def bfs2zippath(bfs, TMPDIR):
    """Convert BFS ID to full path."""
    return f'{TMPDIR}/{bfs}.osm.zip'
    

if __name__ == '__main__':
    start_all = time.monotonic()

    description="""
This software creates modified copies of GWR datasets (obtainable from http://qa.poole.ch/addresses/GWR/ ) in order to simplify their import into OpenStreetMap.

The software works in two (mutually exclusive) modes: "onlinemode" and "filemode".

For both modes you will need to know the BFS (Bundesamt für Statistik) number of the municipality(s) that you are interested in. You can find them on their Wikipedia pages.

For more information read the README.md file.
"""
    # https://stackoverflow.com/a/17909525

    parser = argparse.ArgumentParser(description=description)

    parser.add_argument("--gwrout", type=str, help="Name of output file (ending in .osm or .osm.pbf)", default='gwrout.osm')
    parser.add_argument("--num-workers", type=int, help="The number of processes for reading OSM data", required=False)

    subparsers = parser.add_subparsers(dest='mode') # https://stackoverflow.com/a/9286586

    parser_onlinemode = subparsers.add_parser('onlinemode', help='Get existing addresses online (using Overpass API)')
    parser_filemode = subparsers.add_parser('filemode', help='Provide existing addresses by file (usually slower)')

    parser_filemode.add_argument("--osm", type=argparse.FileType('r'), nargs='+', help="Name of one or multiple OSM files with existing address data, e.g. switzerland-exact.osm.pbf from https://planet.osm.ch/", required=True)
    parser_filemode.add_argument("--gwrin", type=argparse.FileType('r'), nargs='+', help="Name of one or multiple GWR files in .osm format from http://qa.poole.ch/addresses/GWR/", required=True)

    parser_onlinemode.add_argument('--bfs', type=int, nargs='+', help='BFS number(s) of the municipality(s) of interest', required=True)

    args = parser.parse_args()

    if os.path.exists(args.gwrout):
        logger.warning(f'File \'{args.gwrout}\' exists.')
        sys.exit(1)

    TMPDIR = f'{os.getcwd()}/gwr-filter-tmp'
    pathlib.Path(TMPDIR).mkdir(parents=True, exist_ok=True)

    OVERPASS_FILE = f'{TMPDIR}/overpass-temp.osm'

    if args.mode == 'onlinemode':
        # get data from overpass query (args.bfs)
        logger.info('Going to start download from Overpass API...')
        start_overpass = time.monotonic()
        download_from_overpass(args.bfs, OVERPASS_FILE)
        stop_overpass = time.monotonic()
        logger.info(f'Downloading from Overpass API took {truncate(stop_overpass-start_overpass)} s')

        logger.info('Going to start download from GWR...')
        start_gwr_download = time.monotonic()
        download_from_gwr(args.bfs, TMPDIR)
        stop_gwr_download = time.monotonic()
        logger.info(f'Downloading GWR data took {truncate(stop_gwr_download-start_gwr_download)} s')

    global_storage = AddressStorage()

    logger.info("Going to read OSM data...")

    start1 = time.monotonic()
    num_workers = args.num_workers if args.num_workers else max(1,os.cpu_count()//2)
    if args.mode == 'onlinemode':
        osm_files = [OVERPASS_FILE]
    else: # filemode
        osm_files = [f.name for f in args.osm]
    for f in osm_files:
        with Pool(processes=num_workers) as pool:
            w = num_workers
            myargs = zip([x for x in range(w)], [w]*w, [f]*w)
            #import pdb; pdb.set_trace()
            for result in pool.starmap(work, myargs):
                start_merge = time.monotonic()
                global_storage.merge(result)
                stop_merge = time.monotonic()
    stop1 = time.monotonic()
    logger.info(f"Reading OSM data took {truncate(stop1-start1)} s")

    logger.info(f'Some statistics about the OSM data:')
    num_addresses = global_storage.num_addresses()
    num_individual_addresses = global_storage.num_individual_addresses()
    avg = num_individual_addresses / num_addresses
    logger.debug(f"Number of address hashes={num_addresses}")
    logger.debug(f"Average number of entries per address hash={truncate(avg)}")

    logger.info("Going to read/write GWR data...")

    start_rw_gwr = time.monotonic()
    w = osmium.SimpleWriter(args.gwrout)
    g = GwrHandler(global_storage, w)
    if args.mode == 'onlinemode':
        gwrin_files = [bfs2osmpath(bfs, TMPDIR) for bfs in args.bfs]
    else: # filemode
        gwrin_files = [file_.name for file_ in args.gwrin]
    for f in gwrin_files:
        g.apply_file(f, locations=False)
    stop_rw_gwr = time.monotonic()

    logger.debug(f"GWR stats={g.stats}")

    logger.info(f"Reading/writing GWR took {truncate(stop_rw_gwr-start_rw_gwr)} s")
    logger.info(f"Running everything took {truncate(time.monotonic()-start_all)} s")
