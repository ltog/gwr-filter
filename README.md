# gwr-filter

This software creates modified copies of GWR datasets in order to simplify their import into OpenStreetMap.

GWR stands for Gebäude- und Wohnungsregister = Federal Register of Buildings and Dwellings. An extract of it is obtainable from <http://qa.poole.ch/addresses/GWR>.

The following entries will be discarded:

- Addresses already existing in OpenStreetMap.
- Addresses consisting of housenumbers with dots, e.g. `12.2`. (Such housenumbers are given to garages, etc.)
- Addresses with housenumbers `[object Object]`. (These are artifacts of the extraction pipeline of GWR data.)

The software works in two (mutually exclusive) modes: "onlinemode" and "filemode". If you are unsure about which one to choose, use onlinemode! It's much faster and more convenient to use.

For both modes you will need to know the BFS (Bundesamt für Statistik) number of the municipality(s) that you are interested in. You can find them on their Wikipedia pages.

## Initial setup

This software is written in Python. It won't need installation in a strict sense, but best-practice is to use a so-called virtual environment. This process downloads needed libraries in a dedicated directory and prevents messing up other Python projects. The setup process is described for Ubuntu.

Install pip (Python package manager):

    sudo apt update
    sudo apt install python3-pip
    sudo apt install build-essential cmake libboost-dev libexpat1-dev zlib1g-dev libbz2-dev # pyosmium dependencies according to https://github.com/osmcode/pyosmium

Install virtualenv package using pip:

    pip3 install virtualenv

Clone this repository:

    git clone https://gitlab.com/ltog/gwr-filter.git

Change directory:

    cd gwr-filter/

Initialize virtual environment in directory `venv/`:

    virtualenv venv/

Activate virtual environment:

    source venv/bin/activate

Install dependencies of this software:

    pip install -r requirements.txt

Deactivate virtual environment:

    deactivate

## Running the software

### Initial step

Activate virtual environment:

    source venv/bin/activate

### Using onlinemode

The most simple call using onlinemode is:

    ./gwr-filter.py onlinemode --bfs 297

This will write a new (filtered) file called `gwrout.osm` in the current working directory for municipality 297. All necessary data will be downloaded automatically.

### Using filemode

You need to provide (at least) two files:

- A file with current OSM (address) data (parameter: `--osm`). You might get one at <https://planet.osm.ch>
- A file with GWR data (parameter `--gwrin`). A suitable extract of it is obtainable from <http://qa.poole.ch/addresses/GWR>.

The most simple call using filemode is:

    ./gwr-filter.py filemode --osm switzerland-exact.osm.pbf --gwrin 297.osm

This will write a new (filtered) file called `gwrout.osm` in the current working directory for municipality 297.

### Supplying multiple arguments

You can specify multiple numbers/files for arguments `--bfs`, `--osm` and `--gwrin` by separating them with spaces.

### Optional arguments

You can specify another path for your output file using `--gwrout`. It must end in either `.osm` (for writing XML format) or `.osm.pbf` (for writing PBF format).

gwr-filter.py reads files in parallel. You can specify the number of processes to be used using `--num-workers`. This is primarily useful when using filemode. The default value is half the number of your available CPU cores, which gave good benchmark results on the author's machine.

### Synopsis of supported arguments

```
gwr-filter.py [--gwrout GWROUT] [--num-workers NUM_WORKERS] filemode --osm OSM [OSM ...] --gwrin GWRIN [GWRIN ...]

gwr-filter.py [--gwrout GWROUT] [--num-workers NUM_WORKERS] onlinemode --bfs BFS [BFS ...]

gwr-filter.py -h
```

You can get an automatically generated synopsis of supported arguments by executing:

    ./gwr-filter.py -h

However, this output is lacking arguments specific for onlinemode (or filemode respectively). You can see them by typing either

    ./gwr-filter.py filemode

or

    ./gwr-filter.py onlinemode

### Closing step

Deactivate virtual environment:

    deactivate

## Known bugs

- Distance calculations use a very crude approximation
- Addresses in OSM that are defined using relations `associatedStreet` and `street` are not yet recognized
